/* eslint no-unused-vars: 'off'*/

import React, { useState, useEffect } from 'react';
import {createHistory, dumpHistory, sectoralBalance} from './balanceSheetTransform'
import scenario1 from './scenario1'
import * as d3 from "d3"
import _ from 'lodash'
import slugify from 'slugify'


const vizConf = (() => {

    const ledgerWidth = 1200
    const ledgerHeight = 300
    const groundY = ledgerHeight / 2
    const balanceSheetWidth = 210
    const agentIcontHeight = 60

    const coinRadius = ledgerWidth / 75
    const marginCoinHorizontal = 12

    return {
        groundY,
        ledgerHeight,
        ledgerWidth,
        assetUnitHeight: 20,
        assetSliceTopMargin: 15,
        assetSliceLeftMargin: 5,
        balanceSheetHeaderHeight: 40,
        balanceSheetWidth,
        balanceSideWidth: balanceSheetWidth / 2,
        spaceBetweenBalanceSheets: 100,
        agentIcontHeight,
        balanceSheetHeight: 2 * coinRadius + marginCoinHorizontal + 2,
        agenIcontWidth: ledgerWidth / 10,
        marginLedgerTop:10,
        sceneMarginLeft: 4,
        sceneMarginTop: 50
    }
})()

const durationExpandSlice = 1000



const computeBalaceSheetsDataForStep = (scenario, currentStepIdx) => {

    const agentCount = scenario.balanceSheets.length

    const [assetsPalette, liabilitiesPalette] = [
        ["green", "white"],
        ["red", "white"]
    ]
    .map(range => d3.scaleLinear().domain([0, agentCount]).range(range))
    .map(color => _
        .chain(scenario.balanceSheets)
        .map((bs, idx) => [bs.symbol, color(idx)])
        .fromPairs()
        .value()
    )

    const currentStep = scenario.balanceHistory[currentStepIdx]

    return scenario.balanceSheets.map((bs0, idx) => {

        const balancesForThisSheet =  _.find(currentStep, bs => bs.balanceSheetId === bs0.symbol)

        const sumBalanceFunc = balances => _.sumBy(balances, b => b.balance)
        const totalAssets = sumBalanceFunc([...balancesForThisSheet.financialAssets, ...balancesForThisSheet.realAssets])
        const totalLiabilities = sumBalanceFunc(balancesForThisSheet.financialLiabilities)
        const maxTotalAssetsOrLiab = _.max([totalAssets, totalLiabilities])


        const slicesGen = () => {

            const computeSlices = (assetsOrLiability, cx, palette, sliceTypeCode) => {

                const slices = assetsOrLiability.filter(a => a.balance).map(assetOrLiability => {

                    return {
                        id: slugify(`${sliceTypeCode}-zaz-${balancesForThisSheet.balanceSheetId}-${assetOrLiability.symbol}`),
                        sliceTypeCode,
                        ownerSymbol: bs0.symbol,
                        data: assetOrLiability,
                        ...assetOrLiability,
                        height: vizConf.assetUnitHeight * assetOrLiability.balance,
                        width: vizConf.balanceSheetWidth / 2,
                        cx,
                        fill: palette[assetOrLiability.symbol]
                    }
                })

                const nfa = totalAssets - totalLiabilities

                if((nfa > 0 && sliceTypeCode === 'l' )|| (nfa < 0 && sliceTypeCode === 'a' )) {
                    // NFA slice:
                    slices.push({
                        id: slugify(`nfa-zaz-${balancesForThisSheet.balanceSheetId}`),
                        sliceTypeCode: 'nfa',
                        ownerSymbol: bs0.symbol,
                        balance: nfa,
                        height: vizConf.assetUnitHeight * Math.abs(nfa),
                        width: vizConf.balanceSheetWidth / 2,
                        cx: nfa < 0 ?  0 : vizConf.balanceSheetWidth / 2,
                        fill: '#ffff0054'
                    })
                }

                const slicesWithCY = _.reduce(
                    slices,
                    (acc, nextSlice) => {
                        if(acc.length === 0) {
                            return [{
                                ...nextSlice,
                                cy: vizConf.balanceSheetHeaderHeight
                            }]
                        }

                        const lastSlice = _.last(acc)

                        return [
                            ...acc,
                            {
                                ...nextSlice,
                                cy: lastSlice.cy + lastSlice.height
                            }
                        ]
                    },
                    []
                )

                return slicesWithCY
            }

            return [
                ...computeSlices(balancesForThisSheet.financialAssets, 0, assetsPalette,'a'),
                //realAssets,
                ...computeSlices(balancesForThisSheet.financialLiabilities, vizConf.balanceSheetWidth / 2, liabilitiesPalette,'l')
            ]
        }

        const slices = slicesGen()

        return {
            data: bs0,
            slices,
            totalAssets,
            totalLiabilities,
            nfa: totalAssets - totalLiabilities,
            id: `balance-sheet-${bs0.symbol}`,
            symbol: bs0.symbol,
            x: idx * (vizConf.balanceSheetWidth + vizConf.spaceBetweenBalanceSheets) + vizConf.sceneMarginLeft,
            y: vizConf.sceneMarginTop,
            width: vizConf.balanceSheetWidth,
            height: maxTotalAssetsOrLiab*vizConf.assetUnitHeight + vizConf.balanceSheetHeaderHeight
        }
    })
}

const getOtherSlice = (slice, balaceSheetsDataForStep) => {

    const findOtherSlice = otherTypeCode => _
        .chain(_.find(balaceSheetsDataForStep, bs => bs.symbol === slice.symbol).slices)
        .filter(s => s.sliceTypeCode === otherTypeCode)
        .find(s => s.symbol === slice.ownerSymbol)
        .value()

    return slice.sliceTypeCode === 'a' ?
        findOtherSlice('l') :
        findOtherSlice('a')
}

const sectoralBalanceConf = (() => {

    return {
        width:700,
        height:200,
        barWidth:30
    }
})()

const sectoralBalanceChart = (scenario) => {

    const svgSectoralBalance = d3.select("#sectoralBalance").append("svg")
        .attr("class","sectoralBalance")
        .attr("width", sectoralBalanceConf.width)
        .attr("height", sectoralBalanceConf.height+8)

    const x = d3.scaleBand()
            .domain(scenario.sectoralBalance.map((z,i) => i))
            .range([0, sectoralBalanceConf.width])
            .padding(0.1)

    const maxBalance = _
        .chain(scenario.sectoralBalance)
        .map(balances => _
            .chain(balances)
            .map(b => b.balance)
            .max()
            .value()
        )
        .max()
        .value()

    const heightScale = d3.scaleLinear()
        .domain([0, maxBalance])
        .range([0, sectoralBalanceConf.height / 2])

    var colorCategories = d3.scaleOrdinal(d3.schemeCategory10)

    const colorsBySectorLabel = _
      .chain(scenario.options.sectoralBalance)
      .map((sector, idx) => [sector.sectorLabel, colorCategories(idx)])
      .fromPairs()
      .value()

    const zeroYPos = sectoralBalanceConf.height / 2

    const yPos = balance =>
        balance < 0 ? zeroYPos : zeroYPos - heightScale(balance)


    svgSectoralBalance.selectAll("rect.sectoralBalanceBar")
        .data(scenario.sectoralBalance)
        .enter()
        .call(e => {
            scenario.options.sectoralBalance.forEach(sector => {

                const sectorInfo = balances =>
                    _.find(balances, b => b.sectorLabel === sector.sectorLabel)

                const el =
                    e.append("rect")
                    .attr("fill", colorsBySectorLabel[sector.sectorLabel])
                    .attr("class", (z,i) => `sectoralBalanceBar bar-${i}`)
                    .attr("width", x.bandwidth())
                    .attr("x", (d,i) => x(i))
                    .attr("y", z => yPos(sectorInfo(z).balance)+4)
                    .attr("height", z => heightScale(Math.abs(sectorInfo(z).balance)))
                    .attr("opacity", 0.65)
                    .attr("rx", 2)


            })
        })

    const cursorLens =
        svgSectoralBalance
        .append("rect")
        .attr("class", "cursorLens")
        .attr("fill", "none")
        .attr("stroke-width",  2)
        .attr("stroke",  "#4b98c5")
        .attr("width", x.bandwidth() + 9)
        .attr("x", x(0))
        .attr("y", 0)
        .attr("height", sectoralBalanceConf.height + 8)
        .attr("rx", 4)

    let lastStepIdx = 0

    return stepIdx => {

        lastStepIdx = stepIdx
        cursorLens
            .transition().duration(durationExpandSlice)
            .attr("x", x(stepIdx) - 5)

        const unHighlight = () =>
            d3.selectAll(".sectoralBalanceBar")
                .attr("opacity", 0.60)

        unHighlight()

        d3.select(`.bar-${stepIdx}`)
            .transition().duration(durationExpandSlice+900)
            .attr("opacity", 1)
            .on('end', () => {
                unHighlight()
                d3.select(`.bar-${lastStepIdx}`).attr("opacity", 1)
            })

    }
}


const update = (scenario, stepIdx) => {

    let svgSceen = d3.select("svg.ledger")

    if(!svgSceen.node()) {

        svgSceen =  d3.select("#ledger").append("svg")
        .attr("class","ledger")
        .attr("width", vizConf.ledgerWidth)
        .attr("height", vizConf.ledgerHeight)
    }

    const balaceSheetsDataForStep = computeBalaceSheetsDataForStep(scenario, stepIdx)

    const balanceSheetSelection = svgSceen.selectAll("g.balanceSheet")
        .data(balaceSheetsDataForStep, c => c.id)

    const balanceSheetEnter = balanceSheetSelection.enter()
        .append("g")
        .attr("class","balanceSheet")
        .attr("id", c => c.id)
        .attr("transform", d => `translate(${d.x}, ${d.y})`)
        .call(g => {
           g.append("rect")
            .attr("class", "balanceSheetContour")
            .attr("width", s => s.width)
            .attr("rx", 6)
            .attr("fill", "none")
            .attr("stroke-width",  1.1)
            .attr("stroke",  "black")
            .attr("height", vizConf.balanceSheetHeaderHeight)
            .transition().duration(durationExpandSlice)
            .attr("height", s => s.height)

           g.append("text")
            .attr("dy", 15)
            .attr("text-anchor", "middle")
            .attr("dx", vizConf.balanceSheetWidth / 2)
            .attr("cursor","default")
            .text(c => c.data.name);

            [["Assets:", 0],
             ["Liabilities:", vizConf.balanceSheetWidth / 2]
            ].forEach( ([label, dx]) => {
                g.append("text")
                 .attr("dy", 35)
                 .attr("dx", dx+4)
                 .attr("cursor","default")
                 .text(label)
            });

            [[c => c.totalAssets, 0, "totalAssets"],
             [c => c.totalLiabilities, vizConf.balanceSheetWidth / 2, "totalLiabilities"]
            ].forEach( ([label, dx, clasz]) => {
                g.append("text")
                 .attr("class", clasz)
                 .attr("dy", 35)
                 .attr("dx", dx+80)
                 .attr("cursor","default")
                 .text(label)
            })
        })


    balanceSheetSelection.selectAll("text.totalAssets")
        .data(balaceSheetsDataForStep, c => c.id)
        .text(c => c.totalAssets)

    balanceSheetSelection.selectAll("text.totalLiabilities")
        .data(balaceSheetsDataForStep, c => c.id)
        .text(c => c.totalLiabilities)

    balanceSheetSelection.selectAll("rect.balanceSheetContour")
        .data(balaceSheetsDataForStep, c => c.id)
        .transition().duration(durationExpandSlice)
        .attr("height", s => s.height)

    balanceSheetSelection
        .attr("transform", d => `translate(${d.x}, ${d.y})`)

    const sliceRectlFunc = e =>
        e.attr("height", s => s.height)
        .attr("transform", d => `translate(${d.cx}, ${d.cy})`)

    const sliceLabelFunc = e =>
       e.text(s => {
            if(s.sliceTypeCode === 'nfa') {
                if(s.balance > 0)
                    return `<- net: ${s.balance}`
                return `   net: ${s.balance} ->`
            }
            return `${s.symbol}:  ${s.balance}`
        })
        .attr("fill", s => {
            if(s.sliceTypeCode === 'nfa') {
                if(s.balance > 0)
                    return "green"
                return "red"
            }
            return "black"
        })
        .attr("cursor","default")
        .attr("font-weight", "bold")
        .transition().duration(durationExpandSlice)
        .attr("dx", c => c.cx + vizConf.assetSliceLeftMargin + 10)
        .attr("dy", c => c.cy + vizConf.assetSliceTopMargin + c.height/2 - 12)

    const assetSliceSelection =
        svgSceen.selectAll("g.balanceSheet")
        .data(balaceSheetsDataForStep)
        .selectAll("rect.slice")
        .data(bs => bs.slices, d => d.id)

    const thisSliceAndOtherSlice = s => [
        d3.select(`#${s.id}`),
        d3.select(`#${getOtherSlice(s, balaceSheetsDataForStep).id}`)
    ]

    assetSliceSelection.enter()
        .append("rect")
        .attr("class","slice")
        .attr("id", s => s.id)
        .attr("rx", 4)
        .attr("width", s => s.width)
        .attr("fill", s => s.fill)
        .attr("stroke-width",  4)
        .on("mouseout", s => {

            if(s.sliceTypeCode === 'nfa') {
                return
            }

            thisSliceAndOtherSlice(s).forEach(e =>
               e.attr("stroke",  "none")
            )
        })
        .on("mouseover", s => {

            if(s.sliceTypeCode === 'nfa') {
                return
            }

            thisSliceAndOtherSlice(s).forEach(e =>
               e.attr("stroke",  "black")
            )
        })
        .attr("transform", d => `translate(${d.cx}, ${d.cy})`)
        .transition().duration(durationExpandSlice)
        .call(sliceRectlFunc)

    assetSliceSelection.enter()
        .append("text")
        .attr("class","sliceLabel")
        .attr("dx", c => c.cx + vizConf.assetSliceLeftMargin+10)
        .attr("dy", c => c.cy + vizConf.assetSliceTopMargin)
        .call(sliceLabelFunc)

    const slices = _.flatMap(balaceSheetsDataForStep, bs => bs.slices)

    svgSceen.selectAll("rect.slice")
        .data(slices, c => c.id)
        .exit()
        .transition().duration(durationExpandSlice)
        .attr("height", 0)
        .remove()

    svgSceen.selectAll("rect.slice")
        .data(slices, c => c.id)
        .transition().duration(durationExpandSlice)
        .call(sliceRectlFunc)

    svgSceen.selectAll("text.sliceLabel")
        .data(slices, c => c.id)
        .call(sliceLabelFunc)
        .exit()
        .remove()
}

if (module.hot) {

    let doc = null

    try {
        doc = document
    }
    catch (e) {}

    if(doc && doc.getElementsByClassName("ledger").length) {
        update()
    }

    module.hot.accept()
}


export default function BalanceSheetViz() {

    const [step, setStep] = useState(0);
    const [nextDisabled, setNextDisabled] = useState(false)
    const [scenario, setScenario] = useState(null)
    const [sectoralBalanceCursor, setSectoralBalanceCursor] = useState(null)


    useEffect(() => {
        const sc = createHistory(scenario1)
        //dumpHistory(scenario)
        setScenario(sc)
        update(sc, 0)
        setSectoralBalanceCursor({moveTo: sectoralBalanceChart(sc)})
    }, [])


    const moveStep = incr => {
        const nextStep = step + incr
        update(scenario, nextStep)
        sectoralBalanceCursor.moveTo(nextStep)
        setStep(nextStep)
        setNextDisabled(nextStep >= scenario.historyOfChanges.length)
    }

    if(!scenario) {
        return <span>loading...</span>
    }

    return (
        <div className="container">
            <div className="columns is-centered">
                <div className="column is-half">
                    <div id="ledger"></div>
                </div>
            </div>
            <nav className="level">
                <p className="level-item has-text-centered">
                    <button className="button" disabled={step === 0} onClick={() => moveStep(-1)}>PREVIOUS</button>
                    <button className="button" disabled={nextDisabled} onClick={() => moveStep(1)}>NEXT</button>
                </p>
            </nav>
            <div className="columns is-centered">
                <div className="column is-half has-text-centered">
                    <div id="carousel-demo" className="carousel">
                        <div className={"item-0"}>Le début des temps</div>
                        {scenario.historyOfChanges.map((step_, idx) =>

                            step === idx ?
                            <div key={`expl-${idx}`} className={`ìtem-${idx + 1}`} stylez={{ maxWidth: 400 }}>
                                <span className="title is-5">
                                    {step_.explanation}
                                </span>
                            </div>: null
                        )}
                    </div>
                </div>
            </div>
            <div className="columns is-centered">
                <div className="column is-half">
                    <div id="sectoralBalance"></div>
                </div>
            </div>
    </div>
    )
}
