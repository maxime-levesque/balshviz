
import _ from 'lodash'


export const createHistory = scenario => {

    const initialBalanceSheets = scenario.balanceSheets.map(bs => ({
        ...{balanceSheetId: bs.symbol},
        ..._.chain(bs)
             .pick(['financialAssets', 'realAssets','financialLiabilities'])
             .mapValues(slices => slices.map(slice => ({symbol: slice.symbol, balance:0})))
             .value()
    }))

    const z =  _.reduce(
        scenario.historyOfChanges,
        (res, changes) => {

            const lastStep = _.last(res)

            const nextStep = lastStep.map(bs => {

                const changesForThisBs = _.find(changes.opsForEachBs, ops => ops.balanceSheet === bs.balanceSheetId)

                if(! changesForThisBs) {
                    return {...bs}
                }

                const r = _.chain(bs)
                 .toPairs()
                 .map(([key, deltas]) => {

                    if(! _.find(['financialAssets', 'realAssets','financialLiabilities'], s => s === key))    {
                        return [key, deltas]
                    }

                    return [
                        key,
                         deltas.map(({symbol, balance}) => {

                            const v1 = changesForThisBs.deltas[key]

                            if(! v1) {
                                return {symbol, balance}
                            }

                            const v2 = _.find(changesForThisBs.deltas[key], c => c.symbol === symbol)

                            if(!v2) {
                                return {symbol, balance}
                            }

                            return {symbol, balance: balance + v2.d}
                        })
                    ]
                 })
                 .fromPairs()
                 .value()

                 return r
            })

            return [...res, nextStep]
        },
       [initialBalanceSheets]
    )

    const res = {
        ...scenario,
        balanceHistory: z
    }

    return {
        ...res,
        sectoralBalance: sectoralBalance(res),
        sectoralBalanceNets: sectoralBalanceNets(res)
    }
}

const sectoralBalance = scenario =>
    scenario.balanceHistory.map(step => {

        const findBsInStep = symbol => _
            .find(step, s => s.balanceSheetId === symbol)

        return _.map(scenario.options.sectoralBalance, ({sectorLabel, agentSymbols}) => ({
            sectorLabel,
            balance: _.sumBy(agentSymbols, agentSymbol =>
                _.sumBy(findBsInStep(agentSymbol).financialAssets, b => b.balance) -
                _.sumBy(findBsInStep(agentSymbol).financialLiabilities, b => b.balance))
        }))
    })

const sectoralBalanceNets = scenario =>
    scenario.balanceHistory.map(step => {

        const findBsInStep = symbol => _
            .find(step, s => s.balanceSheetId === symbol)

        return _.map(scenario.options.sectoralBalance, ({sectorLabel, agentSymbols}) => ({
            sectorLabel,
            balances: _
                .chain(agentSymbols).map(agentSymbol =>([
                    agentSymbol,
                    _.sumBy(findBsInStep(agentSymbol).financialAssets, b => b.balance) -
                    _.sumBy(findBsInStep(agentSymbol).financialLiabilities, b => b.balance)
                ]))
                .fromPairs()
                .value()
        }))
    })

export const dumpHistory = scenario =>
    scenario.balanceHistory.map(step =>
        step.map(bs => [
            bs.balanceSheetId,
            _.sumBy(bs.financialAssets, b => b.balance),
            _.sumBy(bs.financialLiabilities, b => b.balance)
        ]).join(",")
    )
