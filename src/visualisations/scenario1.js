


const scenario1 = {
    "options": {
        "sectoralBalance": [
            {
                "sectorLabel": "State",
                "agentSymbols": [
                    "$"
                ]
            },
            {
                "sectorLabel": "Non State",
                "agentSymbols": [
                    "b1",
                    "bo"
                ]
            }
        ]
    },
    "balanceSheets": [
        {
            "name": "State",
            "symbol": "$",
            "financialAssets": [
                {
                    "symbol": "b1",
                    "idx": 0
                },
                {
                    "symbol": "bo",
                    "idx": 1
                }
            ],
            "realAssets": [
                {
                    "symbol": "R",
                    "idx": 2
                }
            ],
            "financialLiabilities": [
                {
                    "symbol": "b1",
                    "idx": 3
                },
                {
                    "symbol": "bo",
                    "idx": 4
                }
            ]
        },
        {
            "name": "Bank",
            "symbol": "b1",
            "financialAssets": [
                {
                    "symbol": "$",
                    "idx": 5
                },
                {
                    "symbol": "bo",
                    "idx": 6
                }
            ],
            "realAssets": [
                {
                    "symbol": "R",
                    "idx": 7
                }
            ],
            "financialLiabilities": [
                {
                    "symbol": "$",
                    "idx": 8
                },
                {
                    "symbol": "bo",
                    "idx": 9
                }
            ]
        },
        {
            "name": "Bob",
            "symbol": "bo",
            "financialAssets": [
                {
                    "symbol": "$",
                    "idx": 10
                },
                {
                    "symbol": "b1",
                    "idx": 11
                }
            ],
            "realAssets": [
                {
                    "symbol": "R",
                    "idx": 12
                }
            ],
            "financialLiabilities": [
                {
                    "symbol": "$",
                    "idx": 13
                },
                {
                    "symbol": "b1",
                    "idx": 14
                }
            ]
        }
    ],
        "historyOfChanges": [
            {
                "explanation": "Bob receives a payment of 10 from the state",
                "opsForEachBs": [
                    {
                        "balanceSheet": "$",
                        "deltas": {
                            "financialLiabilities": [
                                {
                                    "symbol": "bo",
                                    "d": 10
                                }
                            ]
                        }
                    },
                    {
                        "balanceSheet": "bo",
                        "deltas": {
                            "financialAssets": [
                                {
                                    "symbol": "$",
                                    "d": 10
                                }
                            ]
                        }
                    }
                ]
            },
            {
                "explanation": "Bob deposits 4 in a his new bank account",
                "opsForEachBs": [
                    {
                        "balanceSheet": "$",
                        "deltas": {
                            "financialLiabilities": [
                                {
                                    "symbol": "b1",
                                    "d": 4
                                },
                                {
                                    "symbol": "bo",
                                    "d": -4
                                }
                            ]
                        }
                    },
                    {
                        "balanceSheet": "b1",
                        "deltas": {
                            "financialAssets": [
                                {
                                    "symbol": "$",
                                    "d": 4
                                }
                            ],
                            "financialLiabilities": [
                                {
                                    "symbol": "bo",
                                    "d": 4
                                }
                            ]
                        }
                    },
                    {
                        "balanceSheet": "bo",
                        "deltas": {
                            "financialAssets": [
                                {
                                    "symbol": "$",
                                    "d": -4
                                },
                                {
                                    "symbol": "b1",
                                    "d": 4
                                }
                            ]
                        }
                    }
                ]
            },
            {
                "explanation": "Bob pays 3 in taxes via his bank account",
                "opsForEachBs": [
                    {
                        "balanceSheet": "$",
                        "deltas": {
                            "financialLiabilities": [
                                {
                                    "symbol": "b1",
                                    "d": -3
                                }
                            ]
                        }
                    },
                    {
                        "balanceSheet": "b1",
                        "deltas": {
                            "financialAssets": [
                                {
                                    "symbol": "$",
                                    "d": -3
                                }
                            ],
                            "financialLiabilities": [
                                {
                                    "symbol": "bo",
                                    "d": -3
                                }
                            ]
                        }
                    },
                    {
                        "balanceSheet": "bo",
                        "deltas": {
                            "financialAssets": [
                                {
                                    "symbol": "b1",
                                    "d": -3
                                }
                            ]
                        }
                    }
                ]
            },
            {
                "explanation": "Bob pays 2 in taxes using cash",
                "opsForEachBs": [
                    {
                        "balanceSheet": "$",
                        "deltas": {
                            "financialLiabilities": [
                                {
                                    "symbol": "bo",
                                    "d": -2
                                }
                            ]
                        }
                    },
                    {
                        "balanceSheet": "bo",
                        "deltas": {
                            "financialAssets": [
                                {
                                    "symbol": "$",
                                    "d": -2
                                }
                            ]
                        }
                    }
                ]
            },
            {
                "explanation": "Bob gets a loan of 5 from the bank",
                "opsForEachBs": [
                    {
                        "balanceSheet": "b1",
                        "deltas": {
                            "financialAssets": [
                                {
                                    "symbol": "bo",
                                    "d": 5
                                }
                            ],
                            "financialLiabilities": [
                                {
                                    "symbol": "bo",
                                    "d": 5
                                }
                            ]
                        }
                    },
                    {
                        "balanceSheet": "bo",
                        "deltas": {
                            "financialAssets": [
                                {
                                    "symbol": "b1",
                                    "d": 5
                                }
                            ],
                            "financialLiabilities": [
                                {
                                    "symbol": "b1",
                                    "d": 5
                                }
                            ]
                        }
                    }
                ]
            },
            {
                "explanation": "Bob pays 1 of interest to the bank using cash",
                "opsForEachBs": [
                    {
                        "balanceSheet": "$",
                        "deltas": {
                            "financialLiabilities": [
                                {
                                    "symbol": "b1",
                                    "d": 1
                                },
                                {
                                    "symbol": "bo",
                                    "d": -1
                                }
                            ]
                        }
                    },
                    {
                        "balanceSheet": "b1",
                        "deltas": {
                            "financialAssets": [
                                {
                                    "symbol": "$",
                                    "d": 1
                                }
                            ]
                        }
                    },
                    {
                        "balanceSheet": "bo",
                        "deltas": {
                            "financialAssets": [
                                {
                                    "symbol": "$",
                                    "d": -1
                                }
                            ]
                        }
                    }
                ]
            },
            {
                "explanation": "Bob refunds his loan with 2 using cash",
                "opsForEachBs": [
                    {
                        "balanceSheet": "$",
                        "deltas": {
                            "financialLiabilities": [
                                {
                                    "symbol": "b1",
                                    "d": 2
                                },
                                {
                                    "symbol": "bo",
                                    "d": -2
                                }
                            ]
                        }
                    },
                    {
                        "balanceSheet": "b1",
                        "deltas": {
                            "financialAssets": [
                                {
                                    "symbol": "$",
                                    "d": 2
                                },
                                {
                                    "symbol": "bo",
                                    "d": -2
                                }
                            ]
                        }
                    },
                    {
                        "balanceSheet": "bo",
                        "deltas": {
                            "financialAssets": [
                                {
                                    "symbol": "$",
                                    "d": -2
                                }
                            ],
                            "financialLiabilities": [
                                {
                                    "symbol": "b1",
                                    "d": -2
                                }
                            ]
                        }
                    }
                ]
            }
        ]
}

export default scenario1