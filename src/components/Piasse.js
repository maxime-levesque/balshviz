import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import Img from "gatsby-image"



// https://www.forbes.com/sites/johntharvey/2020/06/11/why-the-federal-government-is-to-blame-for-the-covid-19-resurgence/?fbclid=IwAR3DvxEqlH3iro49tDqHZmvNJZYC63ENBVzfTX2xVoPEmzVINejWN6wKPjI#53a3f5c52595

const Piasse = () => {
    const data = useStaticQuery(graphql`

    query {
      file(relativePath: { eq: "cft-qc.png" }) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)
    return (
        <div style={{width:"50%"}}>
            <Img fluid={data.file.childImageSharp.fluid}/>
        </div>
    )
}

export default Piasse