import React from "react"


import Helmet from "react-helmet"


const BulmaImports = () => <Helmet
      meta={[
        {
          name: `viewport`,
          content: "width=device-width, initial-scale=1"
        }]}
    >

</Helmet>


export default BulmaImports