// Gatsby supports TypeScript natively!
import React from "react"
import { PageProps, Link, graphql, useStaticQuery} from "gatsby"
import BulmaImports from "../components/bulma";
import SEO from "../components/seo"
import Piasse from "../components/Piasse"
import '../components/style.scss'
import BalanceSheetViz from '../visualisations/BalanceSheetsViz';


const Navbar = () =>
  <nav className="navbar" role="navigation" aria-label="main navigation">


    {false &&<div className="navbar-brand">
        <a className="navbar-item" href="/"></a>

        <a role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false">
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
    </div>}

    <div className="navbar-menu">
        <div className="navbar-start">
          <a className="navbar-item">Mécanique des bilans</a>
        </div>

    </div>
  </nav>


const Entrypoint = ({data}) => {


  const { html, frontmatter: { title } } = data.markdownRemark

  return <div className="container">
    <div className="columns is-mobile">
      <div className="column">
        {Navbar()}
      </div>
    </div>

    <div className="columns is-mobile">
      <div className="column">
        <div className="box">
          <h1 className="title is-1">{title}</h1>

          <BalanceSheetViz/>

        </div>
      </div>
    </div>

    <script
      src={"https://platform.twitter.com/widgets.js"}
    />


    <SEO title="Monnaie Fiscale Québec" />
    <BulmaImports />
  </div>
}


export default Entrypoint


export const pageQuery = graphql`
{
  markdownRemark(frontmatter: {
    key: {
      eq: "PageDAccueil"
    }
  }){
    html,
    frontmatter {
      title,
      key
    }
  }
}
`