---
title: ""
date: "2015-05-28T22:40:32.169Z"
description: ...
key: PageDAccueil
---


Lorsque prendra fin la pandémie du covid-19, nul ne doute que le Québec se retrouvera avec des recettes fiscale réduite, des dépenses accrues, un taux de chômage élevé, et un secteur privé frileux à l’investissement, car c’est bien connu, tout investisseur rationnel n'investit qu’une fois la reprise bien amorcée.

Pour sortir une économie d'une dépression, le remède de la dépense publique a été maintes fois éprouvé, et il ne manque pas de chantiers urgents à réaliser au Québec. Or pour injecter de la dépense public il faut l’ingrédient principale en quantité suffisante: des deniers publics, mais avec une assiette fiscale réduite, la solution traditionnelle est d’émettre des titres de dette, ce qui veut dire en langage commun: emprunter. Il existe cependant un autre moyen plus avantageux.


## La monnaie fiscale

Un <b>billet de monnaie fiscale québécoise</b> ressemblerait à ceci:

...dessin à venir...

_Le Ministère du revenu du Québec accordera un rabais fiscal de $100 sur remise du présent certificat à la personne l’entreprise identifiée ci haut._


Comme vous pouvez lire sur le billet, une personne en possession de ce billet peut le remettre au fisc québécois en échange de rabais fiscal de $100 sur un compte d’impôt ou de TVQ.

Une fraction des dépenses publiques de l’année 2020 pourrait être payée avec des certificats de crédit fiscal québécois transférables CFT.


### Pourquoi un employé ou fournisseur de l'État accepterait d'être payé en partie avec des CFT ?

Imaginez recevoir un billet de $100 de CFT pour rémunération, le billet vous rend plus riche de $100, autant qu’un “vrai” billet de 100 dollars canadiens, car il vous permet une économie de ce montant sur vos charges fiscales. Le billet est transférable, alors même si vous ne payez pas d’impôts, votre épicier, garagiste ou coiffeur les acceptera, pour réduire leur charges fiscales.

### Si le Trésor québécois émet 10 milliards de CFTs, n'y aurait-il pas un manque à gagner de 10 milliards de dollars en recettes fiscales l'année suivante ?

Oui, cependant le trésor pourrait émettre un autre 10 milliards de CFT l'année suivante, ainsi, à chaque années 10 milliards de CFTs seraient annulés et 10 milliards de nouveaux CFT seraient créés, de façon à ce qu'il y ait à tout moment 10 milliards de CFT en circulation. Ce qui représente un emprunt perpétuel de 10 milliards à un taux de 0%, soit l’équivalent d’une création monétaire de 10 milliards.

### Combien de monnaie fiscale pourrait être émise, existe-t-il un danger d’en émettre trop ?

Chaque dollar CFT émis prive le Trésor québécois d’un dollar canadien en revenu fiscal. Cela peut éventuellement poser problème car les achats extérieurs de l’État québécois doivent être réglés en dollars canadiens. La limite supérieur d’émission de CFTs serait atteinte si toutes les dépenses locales sur le territoire du Québec était payées en CFTs. Émettre trop de monnaie fiscale pourrait aussi créer une pression inflationniste, car chaque CFTs émis augmente l’épargne des ménages, et cette épargne pourrait inciter à la dépense au point de créer de l’inflation. En cas d’envolée inflationniste, il serait possible de réduire l’émission de nouveaux CFTs, de façon à réduire la quantité en circulation.

### Pourquoi la monnaie fiscale plutôt que l'emprunt ?

Le Québec emprunte en émettant des bons du trésor, et doit offrir un rendement annuel de 1.25% dans le climat économique actuel, mais ce taux qui est à la merci des marchés pourrait mettre les finances du Québec en mauvaise posture s'il devait trop augmenter.

Imaginons un scénario où le Trésor québécois fait un déficit de 10 milliards (un scénario très optimiste, voir irréaliste, le chiffre 10 est choisi pour la simplicité). Le coût d'emprunt annuel de la dette d'un tel déficit serait de 125 millions, et sur dix ans 1.8 milliards ($1,807,000,000). Ces montants ne représentent que le service de la dette et pourraient être supérieurs si le rendement exigés par les marchés étaient supérieurs au taux actuel de 1.25%.

Imaginons maintenant le scénario où le Trésor québécois paie une partie de ses dépenses en émettant 10 milliards de monnaie fiscale. Concrètement cela voudrait dire que des employés ou fournisseurs de l'État québécois seraient payés en partie avec des certificats de crédits fiscaux transférables (CFT) au lieu d'être payés entièrement en dollars, jusqu'à ce que de 10 milliards de CFT aient été dépensés.

### La monnaie fiscale favorise t-elle l’achat local ?

Oui, car le pouvoir d’achat d’un CFT québécois aurait un pouvoir d’achat réduit, à l’extérieur des frontières du Québec. Une personne à l’extérieur du Québec pourrait accepter un CFTQ comme paiement, mais seulement si elle envisageait l’achat d’un produit québecois. Un CFTQ serait moins attrayant à l’extérieur du Québec qu’un dollar canadien, cela fait que l’achat local est encouragé.


### L’émission de monnaie fiscale a t-elle déjà été essayé ?

De nombreux États et municipalités états-uniens ont émis de la monnaie fiscale lors de la Grande dépression, ils purent ainsi maintenir et palier à l’effondrement de leurs recettes fiscales.

![Billet de monnaie fiscale émis en 1933](./tan-dakota-front.jpg)

Voir: https://nathantankus.substack.com/p/stanch-the-bleeding-from-local-and


### Une monnaie fiscale pourrait-elle émise par une municipalité ?

Oui, tout palier gouvernemental avec un pouvoir de taxation peut émettre de sa monnaie fiscale, mais l’économie d’une municipalité est en général peu diversifiée ce qui réduit les avantages, l'économie est plus diversifiée à l’échelle du Québec. Les municipalités subissent déjà d’énormes pertes de revenus et continueront à en subir. Une émission de CFTs pourrait (et devraient) servir à éponger les déficits municipaux en partie ou en totalité, à tout le moins pour assurer les services municipaux essentiels. L’annulation de déficits municipaux pourrait se faire au prorata de la population desservie par chacune des municipalités.


### Une monnaie fiscale pourrait-elle être émise au palier fédéral ?


Une monnaie fiscale existe déjà au palier fédéral: elle s’appelle le dollar canadien ! Le lecteur surpris par cette affirmation est invité à prendre connaissance de la Théorie monétaire moderne (“Modern Money Theory” en anglais), qui dit en gros que la demande d’une monnaie nationale est créé et maintenu par la taxation, et que la monnaie primaire (billets vers et monnaie centrale) est d’abord créé par la dépense et ensuite détruite par la taxation.


La banque centrale du Canada injectera $5 milliards de liquidités par semaine pour la durée de la pandémie.
https://www.bankofcanada.ca/2020/03/operational-details-for-the-secondary-market-purchases-of-government-of-canada-securities/

... à terminer